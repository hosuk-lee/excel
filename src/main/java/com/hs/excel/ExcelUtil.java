package com.hs.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
    
    public static final String localFile = "C:/app/hosuk/" + "sample" + ".xlsx";
    
    public static void main(String[] args) throws Exception {
        c();
        //a();
        // create();
        readExcel();
        
    }
    
    public static Workbook c() throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        
        List<String> head = new ArrayList<String>();
        head.add("헤더1"); head.add("헤더2"); head.add("헤더3"); head.add("헤더4"); head.add("헤더5");
        createRow(sheet, head, 0);
        
        List<List<String>> body = new ArrayList<List<String>>();
        List<String> row = new ArrayList<String>();
        row.add("row1-data1");row.add("row1-data2");row.add("row1-data3");row.add("row1-data4");row.add("row1-data5");
        body.add(row);
        row = new ArrayList<String>();
        row.add("row2-data1");row.add("row2-data2");row.add("row2-data3");row.add("row2-data4");row.add("row2-data5");
        body.add(row);
        row = new ArrayList<String>();
        row.add("row3-data1");row.add("row3-data2");row.add("row3-data3");row.add("row3-data4");row.add("row3-data5");
        body.add(row);
        int rowSize = body.size();
        for (int i = 0; i < rowSize; i++) {
            createRow(sheet, body.get(i), i + 1);
        }
        
        
        File file = new File(localFile);
        FileOutputStream fos = null;
        fos = new FileOutputStream(file);
        workbook.write(fos);
        return workbook;
    }
    
    private static void createRow(Sheet sheet, List<String> cellList, int rowNum) {
        int size = cellList.size();
        Row row = sheet.createRow(rowNum);

        for (int i = 0; i < size; i++) {
            row.createCell(i).setCellValue(cellList.get(i));
        }
        //Cell cell = row.createCell(i);
        //cell.setCellValue(cellList.get(i));
        //cell.setCellStyle(null);
    }
    
    
    public static Workbook create() {
        XSSFWorkbook xssfWb = null;
        XSSFSheet xssfSheet = null;
        XSSFRow xssfRow = null;
        XSSFCell xssfCell = null;
        try {
            // 행의 갯수
            int rowNo = 0;
            
            xssfWb = new XSSFWorkbook();
            
            // 워크시트 이름 설정
            xssfSheet = xssfWb.createSheet("sheet1");
            
            // make title
            
            XSSFFont font = xssfWb.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 20);
            font.setBold(true);
            
            CellStyle cellStyle = xssfWb.createCellStyle();
            
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);  // 셀에 스타일 지정
           
            xssfCell.setCellValue("타이틀 입니다 1");
            
            
            xssfSheet.setColumnWidth(0, (xssfSheet.getColumnWidth(0)) + (short) 2048);
            cellStyle.setFont(font);
            // 첫행, 마지막행, 첫열, 마지막열 병합
            xssfSheet.addMergedRegion(new CellRangeAddress(1, 3, 0, 4));
            // 행 객체 추가
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 2");
            // 데이터 입력
            //xssfSheet.createRow(rowNo++);
            //xssfRow = xssfSheet.createRow(rowNo++);
            
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 3");
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 4");
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 5");
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 6");
            
            File file = new File(localFile);
            FileOutputStream fos = null;
            fos = new FileOutputStream(file);
            xssfWb.write(fos);
        } catch (Exception e) {
        }
        return xssfWb;
    }
    
    public static Workbook create2() {
        XSSFWorkbook xssfWb = null;
        XSSFSheet xssfSheet = null;
        XSSFRow xssfRow = null;
        XSSFCell xssfCell = null;
        try {
            // 행의 갯수
            int rowNo = 0;
            
            xssfWb = new XSSFWorkbook();
            
            // 워크시트 이름 설정
            xssfSheet = xssfWb.createSheet("sheet1");
            
            // make title
            
            XSSFFont font = xssfWb.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 20);
            font.setBold(true);
            
            CellStyle cellStyle = xssfWb.createCellStyle();
            
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 1");
            
            
            xssfSheet.setColumnWidth(0, (xssfSheet.getColumnWidth(0)) + (short) 2048);
            cellStyle.setFont(font);
            // 첫행, 마지막행, 첫열, 마지막열 병합
            xssfSheet.addMergedRegion(new CellRangeAddress(1, 3, 0, 4));
            // 행 객체 추가
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 2");
            // 데이터 입력
            //xssfSheet.createRow(rowNo++);
            //xssfRow = xssfSheet.createRow(rowNo++);
            
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 3");
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 4");
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 5");
            xssfRow = xssfSheet.createRow(rowNo++);
            // 타이틀 생성
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다 6");
            
            File file = new File(localFile);
            FileOutputStream fos = null;
            fos = new FileOutputStream(file);
            xssfWb.write(fos);
        } catch (Exception e) {
        }
        return xssfWb;
    }
    
    public static void readExcel() {
        try {
            FileInputStream file = new FileInputStream(localFile);
            XSSFWorkbook workbook = new XSSFWorkbook(file);
 
            int rowindex=0;
            int columnindex=0;
            //시트 수 (첫번째에만 존재하므로 0을 준다)
            //만약 각 시트를 읽기위해서는 FOR문을 한번더 돌려준다
            XSSFSheet sheet=workbook.getSheetAt(0);
            //행의 수
            int rows=sheet.getPhysicalNumberOfRows();
            for(rowindex=0;rowindex<rows;rowindex++){
                //행을읽는다
                XSSFRow row=sheet.getRow(rowindex);
                if(row !=null){
                    //셀의 수
                    int cells=row.getPhysicalNumberOfCells();
                    for(columnindex=0; columnindex<=cells; columnindex++){
                        //셀값을 읽는다
                        XSSFCell cell=row.getCell(columnindex);
                        String value="";
                        //셀이 빈값일경우를 위한 널체크
                        if(cell==null){
                            continue;
                        }else{
                            //타입별로 내용 읽기
                            switch (cell.getCellType()){
                            case FORMULA:
                                value=cell.getCellFormula();
                                break;
                            case NUMERIC:
                                value=cell.getNumericCellValue()+"";
                                break;
                            case STRING:
                                value=cell.getStringCellValue()+"";
                                break;
                            case BLANK:
                                value=cell.getBooleanCellValue()+"";
                                break;
                            case ERROR:
                                value=cell.getErrorCellValue()+"";
                                break;
                            default:
                                break;
                            }
                        }
                        System.out.println(rowindex+"번 행 : "+columnindex+"번 열 값은: "+value);
                    }
 
                }
            }
 
        }catch(Exception e) {
            e.printStackTrace();
        }
 
    }
    
    public static Sheet sheet() {
        
        return null;
    }
    
    public static Workbook a() {
        XSSFWorkbook xssfWb = null;
        XSSFSheet xssfSheet = null;
        XSSFRow xssfRow = null;
        XSSFCell xssfCell = null;
        try {
            // 행의 갯수
            int rowNo = 0;
            // XSSFWorkbook 객체 생성
            xssfWb = new XSSFWorkbook();
            
            // 워크시트 이름 설정
            xssfSheet = xssfWb.createSheet("워크 시트1");
            // 폰트 스타일
            XSSFFont font = xssfWb.createFont();
            font.setFontName(HSSFFont.FONT_ARIAL);
            // 폰트 스타일
            font.setFontHeightInPoints((short) 20);
            // 폰트 크기
            font.setBold(true);
            // Bold 설정
            // font.setColor(new XSSFColor(Color.decode("#457ba2")));
            // 폰트 색 지정 z
            // 테이블 셀 스타일
            CellStyle cellStyle = xssfWb.createCellStyle();
            xssfSheet.setColumnWidth(0, (xssfSheet.getColumnWidth(0)) + (short) 2048);
            // 0번째 컬럼 넓이 조절
            cellStyle.setFont(font);
            // cellStyle에 font를 적용
            // cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            // 정렬
            // 셀병합
            xssfSheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 4));
            // 첫행, 마지막행, 첫열, 마지막열 병합
            // 타이틀 생성
            xssfRow = xssfSheet.createRow(rowNo++);
            // 행 객체 추가
            xssfCell = xssfRow.createCell((short) 0);
            // 추가한 행에 셀 객체 추가
            xssfCell.setCellStyle(cellStyle);
            // 셀에 스타일 지정
            xssfCell.setCellValue("타이틀 입니다");
            // 데이터 입력
            xssfSheet.createRow(rowNo++);
            xssfRow = xssfSheet.createRow(rowNo++);
            // 빈행 추가
            // 테이블 스타일 설정
            CellStyle tableCellStyle = xssfWb.createCellStyle();
//        tableCellStyle.setBorderTop((short) 5); 
//        // 테두리 위쪽 
//        tableCellStyle.setBorderBottom((short) 5); 
//        // 테두리 아래쪽 
//        tableCellStyle.setBorderLeft((short) 5); 
//        // 테두리 왼쪽 
//        tableCellStyle.setBorderRight((short) 5); 
            // 테두리 오른쪽
            xssfRow = xssfSheet.createRow(rowNo++);
            xssfCell = xssfRow.createCell((short) 0);
            xssfCell.setCellStyle(tableCellStyle);
            xssfCell.setCellValue("셀1");
            xssfCell = xssfRow.createCell((short) 1);
            xssfCell.setCellStyle(tableCellStyle);
            xssfCell.setCellValue("셀2");
            xssfCell = xssfRow.createCell((short) 2);
            xssfCell.setCellStyle(tableCellStyle);
            xssfCell.setCellValue("셀3");
            xssfCell = xssfRow.createCell((short) 3);
            xssfCell.setCellStyle(tableCellStyle);
            xssfCell.setCellValue("셀4");
            xssfCell = xssfRow.createCell((short) 4);
            xssfCell.setCellStyle(tableCellStyle);
            File file = new File(localFile);
            FileOutputStream fos = null;
            fos = new FileOutputStream(file);
            xssfWb.write(fos);
            if (fos != null)
                fos.close();
        } catch (Exception e) {
        }
        return xssfWb;
    }
}
