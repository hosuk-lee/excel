package com.hs.excel.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hs.excel.ExcelUtil;
import com.hs.excel.service.ExcelService;

@Controller
public class ExcelController {

    // content type
    // .xls 는 최대 65535 행까지 가능
    // .xlsx 는 최대 1048576 행까지 가능
    // xls : application/vnd.ms-excel
    // xlsx : application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    
    @Autowired
    private ExcelService excelService;
    
    @GetMapping("/excel/download")
    public ModelAndView excelDownload(HttpServletRequest request,HttpServletResponse response) throws IOException {
        
         Map<String, Object> excelData = new HashMap<>();
         // excelData.put("workbook", a());
         Workbook a = ExcelUtil.a();
         response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
         ServletOutputStream out = response.getOutputStream();
         a.write(out);
         return new ModelAndView();
    }
    
    @GetMapping("/excel/download2")
    public void excelDownload2(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String fileName = request.getParameter("fileName");
        // 파일 다운로드 처리를 위한 응답헤더 정보 설정하기
        response.setContentType("application/octet-stream"); 
        
        fileName = "123";
        
        // 이미지 = binary data -> html이 아닌 octet stream으로 설정
        response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
        // 다운로드 -> attachment, filename="" -> 저장할 파일명

        // byte[] read = excelService.download(request, response);
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("C:/app/hosuk/excelDownTest.xlsx"));
        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());

        int readBytes = 0;
        while ( (readBytes = bis.read()) != -1 ) {
          bos.write(readBytes);
        }
        //bos.write(read);

        bis.close();
        bos.close();
    }
    
    // @GetMapping(value = "/download/excel", produces = "application/vnd.ms-excel")
    @GetMapping(value = "/api/excelMake")
    public String downloadExcel(Model model) throws UnsupportedEncodingException, ParseException {
        // model.addAttribute("excelMap", excelService.getExcel(criteria));
        System.out.println("22");
        ExcelUtil.a();
        return "123";
    }

}
